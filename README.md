# Profiler back

## Tech
- [SpringMVC](https://spring.io/projects/spring-framework)
- [Spring Boot](https://spring.io/projects/spring-boot)
- [Thymeleaf](https://www.thymeleaf.org/)
- [Swagger](https://swagger.io/)
- [H2](https://www.h2database.com/html/main.html)
- [Logback](https://logback.qos.ch/)
- [JWT](https://jwt.io/)
  
## Useful links
- [Model Mapper](https://www.javadevjournal.com/spring/data-conversion-spring-rest-api/)
- [Thymeleaf](https://www.baeldung.com/thymeleaf-in-spring-mvc)
- [Swagger(Springfox)](https://www.baeldung.com/swagger-2-documentation-for-spring-rest-api)
- [H2](https://www.baeldung.com/spring-boot-h2-database)
- [JWT](https://dev.to/keysh/spring-security-with-jwt-3j76)
- [Loggin](https://www.baeldung.com/spring-boot-logging)
- [MVC Pattern](https://www.journaldev.com/16974/mvc-design-pattern)
- [DTO Pattern](https://thoughts-on-java.org/dto-projections/)
- [DAO Pattern](https://www.baeldung.com/java-dao-pattern)
- [Maven](https://www.baeldung.com/maven)
- [Gitlab](https://about.gitlab.com/2016/03/08/gitlab-tutorial-its-all-connected/)

### Installation

- Install the dependencies
```sh
mvn package
```
- Build project
```sh
mvn eclipse:eclipse
```

## Use cases
### Gestión de entrevistados: Alta, baja, consulta, modificación y listado.
- ALTA:
  * El usuario no puede estar vacío.
  * El @Postmapping debe contener al menos DNI, email, nombre, apellidos, fecha de nacimiento y teléfono.
  * El estado será false a la hora de crearlo.
  * Comprobar/validar DNI, emaikl, nombre, apellidos, teléfono, código postal y fecha de nacimiento.
 
- BAJA:
  * El usuario no puede estar vacío.
  * Ha de confirmarse el borrado del usuario.
  * Comprobar que existe dicho usuario.

- MODIFICACIÓN:
  * El usuario debe al menos disponer de los datos básicos.
  * El usuario debe aparecer en la lista.

- LISTADO: 
  * Comprobar que existen entrevistados.
  * Comprobar que se devuelven todos los datos referentes a los usuarios.

- CONSULTA:
  * EL usuario debe existir.
  * Comprobar que se devuelven todos los datos referentes del usuario a consultar.

### Gestión de tecnologías: Alta, baja, consulta, modificación y listado.
- ALTA:
  * Nombre y tipo de la tecnología no deben estar vacíos.
  * Tipo debe ser back o front.
  * Comprobar que los frameworks sea de tipo array y se ingresan de una determinada manera.

- BAJA:
  * Comprobar que existe la tecnología.
  * Ha de confirmarse el borrado del usuario.
  * La tecnología no debe estar vacía.

- MODIFICACIÓN:
  * La tecnología debe aparecer en la lista.
  * La tecnología debe disponer de los datos básicos.

- LISTADO:
  * Comprobar que existen tecnologías.
  * Comprobar que se devuelven todos los datos.
  * Comprobar que no hay una tecnología sin datos.

- CONSULTA:
  * La tecnología debe existir.
  * Comprobar que se devuelven todos los datos referentes de la tecnología a consultar.


## Entities
 - UserModel (datos en común).
 - InterviewedModel extends UserModel (especialización).
 - TechnologyModel.

## Patterns
- MVC (Spring).
- Repository (Spring).
- DTO (Nuestro).

## Technologies
 - (M) Model => Spring (H2, ModelMapper). 
 - (V) Front =>
   * Thymeleaf. 
 - (C) Controller => JWT, Swagger.
 - Logback en cada clase del back.