package com.plexus.profiler.services;

// import java.util.Iterator;
import java.util.List;
// import java.util.Optional;

import com.plexus.profiler.models.daos.TechnologyDao;
import com.plexus.profiler.models.dtos.TechnologyDto;
import com.plexus.profiler.models.exceptions.NotFoundException;
import com.plexus.profiler.models.exceptions.RecurrentException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TechnologyService{

	@Autowired
	private TechnologyDao daoTechnology;

	// LISTAR.
	public List<TechnologyDto> getAllTechnologies() {

		return daoTechnology.getAllTechnologies();

	}

	// CONSULTA
	public TechnologyDto getTechnologyById(Long id) throws NotFoundException {
		return daoTechnology.getTechnologyById(id);
	}

	// MODIFICACIÓN
	public TechnologyDto updateTechnology(TechnologyDto techP, Long id) {

		return daoTechnology.updateTechnology(techP, id);
	}

	// BAJA
	public void deleteTechnologyById(Long id) throws NotFoundException {
		daoTechnology.deleteTechnologyById(id);
	}

	// BAJA TOTAL
	public void deleteAllTechnologies() throws NotFoundException {
		daoTechnology.deleteAllTechnologies();
	}

	// ALTA
	public TechnologyDto newTechnology(TechnologyDto techP) throws RecurrentException {

		return daoTechnology.newTechnology(techP);

	}

}
