package com.plexus.profiler.services;

import java.util.List;


import com.plexus.profiler.models.daos.InterviewedDao;
import com.plexus.profiler.models.dtos.InterviewedDto;
import com.plexus.profiler.models.exceptions.*;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

/**
 * IntreviewedService
 */
@Service
public class InterviewedService {

	@Autowired
	InterviewedDao interviewedDao;


	// LISTA DE ENTREVISTADOS
	
	public List<InterviewedDto> getAllEntrevieweds() {
		
		return interviewedDao.getAllEntrevieweds();		
	}

	// DEVOLVER UN ENTREVISTADO PIDIENDO EL ID
	
	public InterviewedDto getInterviewedById(Long id) throws MethodArgumentTypeMismatchException, IllegalArgumentException, InterviewedIdNotFoundException {		
		return this.interviewedDao.getInterviewedById(id);
	}

	// NUEVO ENTREVISTADO
	
	public InterviewedDto newInterviewed(InterviewedDto inter) throws Exception{
		//Iterable<InterviewedDto> listaInters = this.interviewedDao.getAllEntrevieweds();
//		for (InterviewedDto interviewed : listaInters) {
//			if (interDto.getDni().equals(interviewed.getDni())) {
//				throw new InterviewedConstraintException();
//			}
//		}
		return this.interviewedDao.newInterviewed(inter);
	}

	// BORRAR ENTREVISTADO POR ID
	
	public void deleteInterviewed(Long id) throws InterviewedIdNotFoundException, MethodArgumentTypeMismatchException, IllegalArgumentException {
		this.interviewedDao.deleteInterviewed(id);
	}

	// BORRAR TODOS LOS ENTREVISTADOS
	
	public void deleteAllIntervieweds() {
		this.interviewedDao.deleteAllIntervieweds();
	}

	// ACTUALIZAR ENTREVISTADO
	
	public InterviewedDto updateInterviewed(InterviewedDto inter, Long id) throws Exception, InterviewedIdNotFoundException, MethodArgumentTypeMismatchException, IllegalArgumentException {
		return this.interviewedDao.updateInterviewed(inter, id);
	}

}