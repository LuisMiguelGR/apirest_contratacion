package com.plexus.profiler.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.plexus.profiler.models.daos.CandidacyDao;
import com.plexus.profiler.models.dtos.CandidacyDto;
import com.plexus.profiler.models.exceptions.CandidacyConstraintException;
import com.plexus.profiler.models.exceptions.CandidacyIdNotFoundException;

/**
 * CandidacyService
 */
@Service
public class CandidacyService {
	
	@Autowired
	CandidacyDao candidacyDao;

	// LISTA DE CANDIDATURAS
	
	public List<CandidacyDto> getAllCandidacies() {
		
		return candidacyDao.getAllCandidacies();		
	}

	// DEVOLVER UNA CANDIDATURA PIDIENDO EL ID
	
	public CandidacyDto getCandidacyById(Long id) throws MethodArgumentTypeMismatchException, IllegalArgumentException, CandidacyIdNotFoundException {		
		return this.candidacyDao.getCandidacyById(id);
	}

	// NUEVA CANDIDATURA
	
	public CandidacyDto newCandidacy(CandidacyDto candiDto) throws CandidacyConstraintException{
		Iterable<CandidacyDto> listaCandis = this.candidacyDao.getAllCandidacies();
		for (CandidacyDto candi : listaCandis) {
			if (candiDto.getId().equals(candi.getId())) {
				throw new CandidacyConstraintException();
			}
		}
		return this.candidacyDao.newCandidacy(candiDto);
	}

	// BORRAR CANDIDATURA POR ID
	
	public void deleteCandidacy(Long id) throws CandidacyIdNotFoundException, MethodArgumentTypeMismatchException, IllegalArgumentException {
		this.candidacyDao.deleteCandidacy(id);
	}

	// BORRAR TODAS LAS CANDIDATURAS
	
	public void deleteAllCandidacies() {
		this.candidacyDao.deleteAllCandidacies();
	}

	// ACTUALIZAR CANDIDATURA
	
	public CandidacyDto updateCandidacy(CandidacyDto candi, Long id) throws CandidacyIdNotFoundException, MethodArgumentTypeMismatchException, IllegalArgumentException {
		return this.candidacyDao.updateCandidacy(candi, id);
	}
}
