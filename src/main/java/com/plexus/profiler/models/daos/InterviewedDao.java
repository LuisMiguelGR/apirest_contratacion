package com.plexus.profiler.models.daos;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.plexus.profiler.models.dtos.InterviewedDto;
import com.plexus.profiler.models.dtos.TechnologyDto;
import com.plexus.profiler.models.entities.Interviewed;
import com.plexus.profiler.models.entities.Technology;
import com.plexus.profiler.models.exceptions.InterviewedConstraintException;
import com.plexus.profiler.models.exceptions.InterviewedIdNotFoundException;
import com.plexus.profiler.models.exceptions.NotFoundException;
import com.plexus.profiler.models.repositories.InterviewedRepository;
import com.plexus.profiler.models.repositories.TechnologyRepository;

import org.jboss.logging.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

/**
 * InterviewedDao
 */
@Component
public class InterviewedDao {

	@Autowired
	InterviewedRepository interRepository;

//	@Autowired
//	TechnologyRepository techRepository;
	@Autowired
	TechnologyDao techDao;

	@Autowired
	ModelMapper modelMapper;

	private static final Logger logger = Logger.getLogger(InterviewedDao.class);

	// LISTA DE ENTREVISTADOS
	@Transactional
	public List<InterviewedDto> getAllEntrevieweds() {
		List<InterviewedDto> resultadoLista = new ArrayList<>();
		Iterable<Interviewed> inters = this.interRepository.findAll();
		Iterable<TechnologyDto> techs = this.techDao.getAllTechnologies();

		for (Interviewed inter : inters) {
			List<Technology> listaTechs = inter.getTecnologias();
//			for(Technology t : techs) {
//				if(listaTechs.contains(t)) {
//					
//					inter.addTechnology(t);
//				}
//			}
			resultadoLista.add(modelMapper.map(inter, InterviewedDto.class));
		}
		// interRepository.findAll().forEach(inter -> lista.add(inter));
		return resultadoLista;
	}

	// DEVOLVER UN ENTREVISTADO PIDIENDO EL ID
	@Transactional
	public InterviewedDto getInterviewedById(Long id)
			throws InterviewedIdNotFoundException, MethodArgumentTypeMismatchException, IllegalArgumentException {
		if (!this.interRepository.findById(id).isPresent()) {
			logger.error("El método getInterviewedById falló! " + new InterviewedIdNotFoundException());
			throw new InterviewedIdNotFoundException();
		}
		Interviewed encontrado = this.interRepository.findById(id).get();
//		encontrado.setTecnologias(null);
		return modelMapper.map(encontrado, InterviewedDto.class);
	}

	// NUEVO ENTREVISTADO
	@Transactional
	public InterviewedDto newInterviewed(InterviewedDto interDto) throws Exception {
		Interviewed inter = modelMapper.map(interDto, Interviewed.class);
		Iterable<Interviewed> listaInters = this.interRepository.findAll();
		List<TechnologyDto> listaTechs = this.techDao.getAllTechnologies();
		for (Interviewed interviewed : listaInters) {
			if (interDto.getDni().equals(interviewed.getDni())) {
				logger.error("El método newInterviewed falló! " + new InterviewedConstraintException());
				throw new InterviewedConstraintException();
			}
		}
		for (Technology t : interDto.getTecnologias()) {			
			if(!this.techDao.getTechnologyById(t.getId()).equals(t)) {
				throw new NotFoundException("");
			}
		}
//		for (Technology t : inter.getTecnologias()) {			
//			if(!this.techDao.getAllTechnologies().contains(t)) {
//				throw new Exception(" La tecnología no existe");
//			}
//		}
		this.interRepository.save(inter);
		return interDto;
	}

	// BORRAR ENTREVISTADO POR ID
	@Transactional
	public void deleteInterviewed(Long id)
			throws InterviewedIdNotFoundException, MethodArgumentTypeMismatchException, IllegalArgumentException {
		if (!this.interRepository.findById(id).isPresent()) {
			logger.error("El método deleteInterviewed falló! " + new InterviewedIdNotFoundException());
			throw new InterviewedIdNotFoundException();
		}
		this.interRepository.deleteById(id);
	}

	// BORRAR TODOS LOS ENTREVISTADOS
	@Transactional
	public void deleteAllIntervieweds() {
		Iterable<Interviewed> lista = this.interRepository.findAll();
		this.interRepository.deleteAll(lista);
	}

	// ACTUALIZAR ENTREVISTADO
	@Transactional
	public InterviewedDto updateInterviewed(InterviewedDto inter, Long id)
			throws Exception {
		if (!this.interRepository.findById(id).isPresent()) {
			logger.error("El método updateInterviewed falló! " + new InterviewedIdNotFoundException());
			throw new InterviewedIdNotFoundException();
		}
		for (Technology t : inter.getTecnologias()) {			
			if(!this.techDao.getTechnologyById(t.getId()).equals(t)) {
				throw new NotFoundException("");
			}
		}
		
		Interviewed interUpdate = this.interRepository.findById(id).get();
		interUpdate.setTecnologias(null);
		interUpdate.setTecnologias(inter.getTecnologias());
		modelMapper.map(inter, interUpdate);
		interUpdate.setId(id);
		Interviewed interNuevo = this.interRepository.save(interUpdate);
		return modelMapper.map(interNuevo, InterviewedDto.class);
	}

}