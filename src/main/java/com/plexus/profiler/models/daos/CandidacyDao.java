package com.plexus.profiler.models.daos;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.jboss.logging.Logger;

import com.plexus.profiler.models.dtos.CandidacyDto;
import com.plexus.profiler.models.entities.Candidacy;
import com.plexus.profiler.models.exceptions.CandidacyConstraintException;
import com.plexus.profiler.models.exceptions.CandidacyIdNotFoundException;
import com.plexus.profiler.models.repositories.CandidacyRepository;

/**
 * CandidacyDao
 */
@Component
public class CandidacyDao {

	@Autowired
	CandidacyRepository candiRepository;
	@Autowired
	ModelMapper modelMapper;

	private static final Logger logger = Logger.getLogger(CandidacyDao.class);
	
	// LISTA DE CANDIDATURAS

	public List<CandidacyDto> getAllCandidacies() {
		List<CandidacyDto> resultadoLista = new ArrayList<>();
		Iterable<Candidacy> candis = this.candiRepository.findAll();
		for (Candidacy candi : candis) {
			resultadoLista.add(modelMapper.map(candi, CandidacyDto.class));
		}
		return resultadoLista;
	}

	// DEVOLVER UNA CANDIDATURA PIDIENDO EL ID

	public CandidacyDto getCandidacyById(Long id)
			throws CandidacyIdNotFoundException, MethodArgumentTypeMismatchException, IllegalArgumentException {
		if (!this.candiRepository.findById(id).isPresent()) {
			logger.error("El método getCandidacyById falló! " + new CandidacyIdNotFoundException());
			throw new CandidacyIdNotFoundException();
		}
		Candidacy encontrada = this.candiRepository.findById(id).get();
		return modelMapper.map(encontrada, CandidacyDto.class);

	}

	// NUEVA CANDIDATURA

	public CandidacyDto newCandidacy(CandidacyDto candiDto) throws CandidacyConstraintException {
		Candidacy candi = modelMapper.map(candiDto, Candidacy.class);
		Iterable<Candidacy> listaCandis = this.candiRepository.findAll();
		for (Candidacy candidacy : listaCandis) {
			if (candiDto.getId().equals(candidacy.getId())) {
				logger.error("El método newCandidacy falló! " + new CandidacyConstraintException());
				throw new CandidacyConstraintException();
			}
		}
		this.candiRepository.save(candi);
		return candiDto;
	}

	// BORRAR CANDIDATURA POR ID

	public void deleteCandidacy(Long id) 
			throws CandidacyIdNotFoundException, MethodArgumentTypeMismatchException, IllegalArgumentException {
		if (!this.candiRepository.findById(id).isPresent()) {
			logger.error("El método deleteCandidacy falló! " + new CandidacyIdNotFoundException());
			throw new CandidacyIdNotFoundException();
		}
		this.candiRepository.deleteById(id);
	}

	// BORRAR TODAS LAS CANDIDATURAS

	public void deleteAllCandidacies() {
		Iterable<Candidacy> lista = this.candiRepository.findAll();
		this.candiRepository.deleteAll(lista);
	}

	// ACTUALIZAR CANDIDATURA

	public CandidacyDto updateCandidacy(CandidacyDto candi, Long id)
			throws CandidacyIdNotFoundException, MethodArgumentTypeMismatchException, IllegalArgumentException
			 {		
		if (!this.candiRepository.findById(id).isPresent()) {
			logger.error("El método updateCandidacy falló! " + new CandidacyIdNotFoundException());
			throw new CandidacyIdNotFoundException();
		}
		Candidacy candiUpdate = this.candiRepository.findById(id).get();
		modelMapper.map(candi, candiUpdate);
		candiUpdate.setId(id);
		Candidacy candiNueva = this.candiRepository.save(candiUpdate);
		return modelMapper.map(candiNueva, CandidacyDto.class);
	}

}
