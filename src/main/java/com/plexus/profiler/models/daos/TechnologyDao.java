package com.plexus.profiler.models.daos;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.plexus.profiler.models.dtos.TechnologyDto;
import com.plexus.profiler.models.entities.Technology;
import com.plexus.profiler.models.exceptions.NotFoundException;
import com.plexus.profiler.models.exceptions.RecurrentException;
import com.plexus.profiler.models.repositories.TechnologyRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TechnologyDao {

    @Autowired
    TechnologyRepository technologyRepository;

    @Autowired
    ModelMapper modelMapper;

    // LISTAR
    public List<TechnologyDto> getAllTechnologies() {
        List<TechnologyDto> lista = new ArrayList<>();
        technologyRepository.findAll().forEach(tech -> lista.add(modelMapper.map(tech, TechnologyDto.class)));
        return lista;
    }

    // CONSULTA
    public TechnologyDto getTechnologyById(Long id) throws NotFoundException {
        if (!technologyRepository.findById(id).isPresent()) {
            throw new NotFoundException(Long.toString(id));
        }
        Technology tech = technologyRepository.findById(id).get();
        return modelMapper.map(tech, TechnologyDto.class);
    }

    // ALTA
    public TechnologyDto newTechnology(TechnologyDto techP) throws RecurrentException {
        Technology tech = modelMapper.map(techP, Technology.class);
        this.technologyRepository.save(tech);
        return techP;
    }

    // BAJA
    public void deleteTechnologyById(Long id) throws NotFoundException {
        if (!this.technologyRepository.findById(id).isPresent()) {
            throw new NotFoundException(Long.toString(id));
        } else {
            this.technologyRepository.deleteById(id);
        }

    }

    // BAJA TOTAL
    public void deleteAllTechnologies() throws NotFoundException {
        // Iterable<Technology> lista = this.technologyRepository.findAll();
        // this.technologyRepository.count()
        if(this.technologyRepository.count() >0 ){
            this.technologyRepository.deleteAll();
        } else{
            throw new NotFoundException("No hay tecnologías almacenadas.", 0);
        }
    }

    // MODIFICACIÓN
    public TechnologyDto updateTechnology(TechnologyDto techP, Long id) throws NotFoundException {
        Technology tech;
        if (!this.technologyRepository.findById(id).isPresent()) {
            throw new NotFoundException(Long.toString(id));
        } else {
            tech = this.technologyRepository.findById(id).get();
            techP.setId(id);
            this.modelMapper.map(techP, tech);
        }
        Technology uno = this.technologyRepository.save(tech);

        return modelMapper.map(uno, TechnologyDto.class);

    }

}
