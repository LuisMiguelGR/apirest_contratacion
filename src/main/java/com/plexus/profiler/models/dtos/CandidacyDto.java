package com.plexus.profiler.models.dtos;

import java.util.Date;
import java.util.List;

import com.plexus.profiler.models.entities.Technology;

public class CandidacyDto {

	private Long id;
	private String titulo;
	private Date fechaInicio;
	private Date fechaFin;
	private List<Technology> requisitos;
	
	
	public CandidacyDto() {
		
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public List<Technology> getRequisitos() {
		return requisitos;
	}
	public void setRequisitos(List<Technology> requisitos) {
		this.requisitos = requisitos;
	}
	
	
}
