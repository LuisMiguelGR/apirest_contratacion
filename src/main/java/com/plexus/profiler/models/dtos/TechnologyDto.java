package com.plexus.profiler.models.dtos;

import java.util.List;
import java.util.Set;

import com.plexus.profiler.models.entities.Interviewed;

/**
 * TechnologyDto
 */

public class TechnologyDto {
    
    private long id;
    private String name;
    private String[] framework;
    private String tipo;
//    private List<Interviewed> intervieweds;
    
    public TechnologyDto() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getFramework() {
        return framework;
    }

    public void setFramework(String[] framework) {
        this.framework = framework;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

//	public List<Interviewed> getIntervieweds() {
//		return intervieweds;
//	}
//
//	public void setIntervieweds(List<Interviewed> intervieweds) {
//		this.intervieweds = intervieweds;
//	}



    
}
