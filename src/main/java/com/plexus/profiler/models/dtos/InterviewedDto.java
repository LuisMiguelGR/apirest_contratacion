package com.plexus.profiler.models.dtos;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.plexus.profiler.models.entities.Technology;

import io.swagger.annotations.ApiModelProperty;


public class InterviewedDto {

	@ApiModelProperty(value = "Id unico de cada entrevistado")
	private Long id;
	
	@ApiModelProperty(value = "Nombre del entrevistado")
	private String nombre;
	
	@ApiModelProperty(value = "Primer apellido del entrevistado")
	private String apellido1;
	
	@ApiModelProperty(value = "Segundo apellido del entrevistado")
	private String apellido2;
	
	@ApiModelProperty(value = "Dni unico de cada entrevistado")
	private String dni;
	
	@ApiModelProperty(value = "Dirección completa del entrevistado")
	private String direccion;
	
	@ApiModelProperty(value = "Fecha de nacimiento del entrevistado")
	private Date fechaNacimiento;
	
	@ApiModelProperty(value = "Email asociado al entrevistado")
	private String email;
	
	@ApiModelProperty(value = "Telefono del entrevistado")
	private Integer telefono;
	
	@ApiModelProperty(value = "Ciudad del entrevistado")
	private String ciudad;
	
	@ApiModelProperty(value = "Provincia del entrevistado")
	private String provincia;
	
	@ApiModelProperty(value = "Codigo postal del entrevistado")
	private Integer codigoPostal;
	
	@ApiModelProperty(value = "Estado en el que se encuentra el entrevistado. Valores permitidos: Entrevistado, Sin entrevistar")
	private String estado;
	
	@ApiModelProperty(value = "El curriculum vitae del entrevistado")
	private String cv;
	
	@ApiModelProperty(value = "Entrevistador que tiene asociado el entrevistado")
	private String entrevistadorAsociado;
	
	@ApiModelProperty(value = "Lista con las entrevistas que sabe usar el entrevistado")
	private List<Technology> tecnologias;
	
	@ApiModelProperty(value = "Observaciones que tiene el entrevistador del entrevistado")
	private String observaciones;

	public InterviewedDto() {
		
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getTelefono() {
		return telefono;
	}

	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public Integer getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(Integer codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCv() {
		return cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public String getEntrevistadorAsociado() {
		return entrevistadorAsociado;
	}

	public void setEntrevistadorAsociado(String entrevistadorAsociado) {
		this.entrevistadorAsociado = entrevistadorAsociado;
	}

	public List<Technology> getTecnologias() {
		return tecnologias;
	}

	public void setTecnologias(List<Technology> tecnologias) {
		this.tecnologias = tecnologias;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

}
