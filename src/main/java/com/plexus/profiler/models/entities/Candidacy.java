package com.plexus.profiler.models.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;

@ApiModel
@Entity
@Table(name = "candidacies")
public class Candidacy implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiParam(required = true, value = "Identificador único de cada candidatura")
	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GenericGenerator(name = "generador", strategy = "com.plexus.profiler.config.IdGenerator")
	@GeneratedValue(generator = "generador")
	private Long id;

	@Column(name = "titulo", nullable = false, length = 50)
	private String titulo;

	@Column(name = "descripcion", nullable = true, length = 255)
	private String descripcion;
	
	@Column(name = "fecha_inicio")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date fechaInicio;

	@Column(name = "fecha_fin")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date fechaFin;

	@ManyToMany(targetEntity = Technology.class)
	@Column(name="tecnologias", nullable=true, length=255)
	private List<Technology> requisitos;

	
	public Candidacy() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public List<Technology> getRequisitos() {
		return requisitos;
	}

	public void setRequisitos(List<Technology> requisitos) {
		this.requisitos = requisitos;
	}

}
