package com.plexus.profiler.models.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.FetchMode;
import org.hibernate.annotations.Fetch;

import com.plexus.profiler.models.dtos.TechnologyDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;

/**
 * Interviewed
 */
@ApiModel
@Entity
@Table(name = "intervieweds")
public class Interviewed extends Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "estado", nullable = false, length = 20)
	private String estado;

	@Column(name = "cv", nullable = true, length = 255)
	private String cv;

	 @JoinTable(
	            name = "intervieweds_technologies",
	            joinColumns = @JoinColumn(name = "interviewed_id", referencedColumnName = "id"),
	            inverseJoinColumns = @JoinColumn(name="tecnologia_id",  referencedColumnName = "id")
	        )
	@ManyToMany(targetEntity=Technology.class, cascade = {CascadeType.REMOVE}, fetch = FetchType.LAZY)	
	private List<Technology> tecnologias;

	@Column(name = "observaciones", nullable = true, length = 255)
	private String observaciones;

	public Interviewed() {

	}
	
//	public void addTechnology(Technology tech) {
//		this.tecnologias.add(tech);
//		tech.getInters().add(this);
//	}
//	
//	public void removeTechnology(Technology tech) {
//		this.tecnologias.remove(tech);
//		tech.getInters().remove(this);
//	}
	
	public List<Technology> getTecnologias() {
		return this.tecnologias;
	}

	public void setTecnologias(List<Technology> tecnologias) {
		this.tecnologias = tecnologias;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCv() {
		return this.cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public void addTechnology(Technology technology) {
		tecnologias.add(technology);
	}

	public String getObservaciones() {
		return this.observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	

}