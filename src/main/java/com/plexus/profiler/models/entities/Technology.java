package com.plexus.profiler.models.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
// import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
// import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.FetchMode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.GenericGenerator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Technology
 */

@Entity
@ApiModel(value = "Technology", description = "Technology model")
@Table(name = "technologies")
public class Technology implements Serializable{
    
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GenericGenerator(name = "generador", strategy = "com.plexus.profiler.config.IdGeneratorTechnologies")
    @GeneratedValue(generator = "generador")
    @ApiModelProperty(value = "Identificador único de cada tecnología.")
    private long id;

    @Column(name = "NAME", nullable = false, unique = true)
    @NotEmpty(message = "Please provide a name")
    @ApiModelProperty(value = "Nombre único de cada tecnología.")
    private String name;

    @Column(name = "FRAMEWORK", columnDefinition = "VARCHAR2(255)", nullable = true)
    @ApiModelProperty(value = "Frameworks de cada tecnología. (Opcionales).")
    private String[] framework;

    @Column(name = "TIPO", nullable = false)
    @NotEmpty(message = "Please provide a tipo")
    @ApiModelProperty(value = "Tipo de capa en la que se emplea la tecnología (Front-end o back-end).")
    private String tipo;
  
//    @ManyToMany
//    private List<Interviewed> intervieweds;


	public Technology() {

    }

//    public List<Interviewed> getInters() {
//		return intervieweds;
//	}
//
//	public void setInters(List<Interviewed> inters) {
//		this.intervieweds = inters;
//	}

	/**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the framework
     */
    public String[] getFramework() {
        return framework;
    }

    /**
     * @param framework the framework to set
     */
    public void setFramework(String[] framework) {
        this.framework = framework;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

   

}
