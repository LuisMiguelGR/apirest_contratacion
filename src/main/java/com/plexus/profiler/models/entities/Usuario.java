package com.plexus.profiler.models.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.plexus.profiler.models.repositories.UsuarioRepository;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;



/**
 * Usuario
 */
@ApiModel
@MappedSuperclass
public class Usuario implements UsuarioRepository, Serializable{


    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "Identificador único de cada interviewed.")
    @Id

    @Column(name="id", nullable=false, unique=true)
    @GenericGenerator(name = "generador", strategy = "com.plexus.profiler.config.IdGenerator")
    @GeneratedValue(generator = "generador")

    private Long id;
    
    @Column(name="nombre", nullable=false, length=50)
    private String nombre;

    @Column(name="apellido1", nullable=true, length=50)
    private String apellido1;
    
    @Column(name="apellido2", nullable=true, length=50)
    private String apellido2;
    
    @Column(name="dni", nullable=false, length=20)
    private String dni;
    
    @Column(name="direccion", nullable=true, length=100)

    private String direccion;

    @Column(name="fecha_nacimiento")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date fechaNacimiento;

    @Column(name="email", nullable=true, length=100)
    private String email;

    
    @Column(name="telefono", nullable=true, length=20)
    private Integer telefono;
    
    @Column(name="ciudad", nullable=true, length=50)
    private String ciudad;
    
    @Column(name="provincia", nullable=true, length=50)
    private String provincia;

    @Column(name="codigo_postal", nullable=true, length=10)
    private Integer codigoPostal;


    public Usuario() {
        
    }

//    public Usuario(Long id, String nombre, String apellido1, String apellido2, String dni, String direccion, /* Date fechaNacimiento, */ String email, Integer telefono, String ciudad, String provincia, Integer codigoPostal) {
//        this.id = id;
//        this.nombre = nombre;
//        this.apellido1 = apellido1;
//        this.apellido2 = apellido2;
//        this.dni = dni;
//        this.direccion = direccion;
//        //this.fechaNacimiento = fechaNacimiento;
//        this.email = email;
//        this.telefono = telefono;
//        this.ciudad = ciudad;
//        this.provincia = provincia;
//        this.codigoPostal = codigoPostal;
//
//    }
    
    // @PrePersist
    // public void preGuardar(){
    //     this.fechaNacimiento = new Date();
    // }


    /**
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return this.apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return this.apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getDni() {
        return this.dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getFechaNacimiento() {
        return this.fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getTelefono() {
        return this.telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public String getCiudad() {
        return this.ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getProvincia() {
        return this.provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public Integer getCodigoPostal() {
        return this.codigoPostal;
    }

    public void setCodigoPostal(Integer codigoPostal) {
        this.codigoPostal = codigoPostal;
    }
    
}