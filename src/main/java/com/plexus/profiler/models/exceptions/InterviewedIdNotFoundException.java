package com.plexus.profiler.models.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;
/**
 * NotFoundInterviewedIdException
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class InterviewedIdNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;
    public static final String DESCRIPTION = "No se encuentra el entrevistado";

    public static final int CODE = 001;

    public InterviewedIdNotFoundException() {
        this("");
    }

    public InterviewedIdNotFoundException(String detail) {
        super(DESCRIPTION + " " + detail + ". CODE: " + CODE);
    }

    
    
}