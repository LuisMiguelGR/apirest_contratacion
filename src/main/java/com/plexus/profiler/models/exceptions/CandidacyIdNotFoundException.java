package com.plexus.profiler.models.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;
/**
 * CandidacyNotFoundIdException
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class CandidacyIdNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;
    public static final String DESCRIPTION = "No se encuentra la candidatura";

    public static final int CODE = 006;

    public CandidacyIdNotFoundException() {
        this("");
    }

    public CandidacyIdNotFoundException(String detail) {
        super(DESCRIPTION + " " + detail + ". CODE: " + CODE);
    }

    
    
}