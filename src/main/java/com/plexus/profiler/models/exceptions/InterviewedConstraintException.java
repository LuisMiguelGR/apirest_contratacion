package com.plexus.profiler.models.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class InterviewedConstraintException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public static final String DESCRIPTION = "Recurso repetido";

    public static final int CODE = 004;

    public InterviewedConstraintException() {
        this("");
    }

    public InterviewedConstraintException(String detail) {
        super(DESCRIPTION + " " + detail + ". CODE: " + CODE);
    }
}
