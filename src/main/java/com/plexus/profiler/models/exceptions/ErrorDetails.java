package com.plexus.profiler.models.exceptions;

import java.util.Date;

/**
 * ErrorDetails
 */
public class ErrorDetails {
    private Date timestamp;
    private String message;
    private String details;
    private int code;

    public ErrorDetails(int code, Date timestamp, String message, String details) {
        this.code = code;
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
    }

    /**
     * @return the timestamp
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the details
     */
    public String getDetails() {
        return details;
    }

    /**
     * @param details the details to set
     */
    public void setDetails(String details) {
        this.details = details;
    }

    /**
     * @return the code
     */

    public int getCode() {
        return this.code;
    }

    /**
     * @param code the code to set
     */

    public void setCode(int code) {
        this.code = code;
    }

}
