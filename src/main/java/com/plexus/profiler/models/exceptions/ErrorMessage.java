package com.plexus.profiler.models.exceptions;

import java.util.Date;


/**
 * ErrorDetails
 */
public class ErrorMessage {

	private int code_status;

	private String error;

    private String description;

    private Date date;

    public ErrorMessage(Exception exception, int code_status){
        this(exception.getClass().getSimpleName(), exception.getMessage(), code_status);
    }

    public ErrorMessage(String error, String description, int code_status){
        this.code_status = code_status;
    	this.error = error;
        this.description = description;
        this.date = new Date();
    }

    /**
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }
 
    public int getCode_Status() {
		return code_status;
	}

	public void setCode_Status(int code) {
		this.code_status = code;
	}
}