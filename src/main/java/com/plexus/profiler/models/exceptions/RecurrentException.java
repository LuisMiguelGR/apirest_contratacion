package com.plexus.profiler.models.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN) // Debería retornar 403
public class RecurrentException extends Exception {

    private static final long serialVersionUID = 1L;

    public RecurrentException(String msg) {
        super("Error: Ya existe el recurso " + msg + ".");
    }

}
