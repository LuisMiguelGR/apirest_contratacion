package com.plexus.profiler.models.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * CandidacyExceptionHandler
 */
@ControllerAdvice
public class CandidacyExceptionHandler extends ResponseEntityExceptionHandler{

	
    @ExceptionHandler(CandidacyIdNotFoundException.class)
    public ResponseEntity<ErrorMessage> notFoundRequest(Exception exception){
    	ErrorMessage errorMessage = new ErrorMessage(exception, HttpStatus.NOT_FOUND.value());
        return new ResponseEntity<>(errorMessage, HttpStatus.NOT_FOUND);
    }
    
    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public ResponseEntity<ErrorMessage> mismatchRequest(Exception exception){
    	ErrorMessage errorMessage = new ErrorMessage(new CandidacyParamArgumentTypeMismatchException(), HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<ErrorMessage> nullIlegalRequest(Exception exception){
    	ErrorMessage errorMessage = new ErrorMessage(new CandidacyNullIlegalArgumentException(),  HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler({CandidacyConstraintException.class})
    public ResponseEntity<ErrorMessage> repeatConstraintRequest(Exception exception){
    	ErrorMessage errorMessage = new ErrorMessage(new CandidacyConstraintException(), HttpStatus.FORBIDDEN.value());
    	return new ResponseEntity<>(errorMessage, HttpStatus.FORBIDDEN);
    }
    
   
    
}