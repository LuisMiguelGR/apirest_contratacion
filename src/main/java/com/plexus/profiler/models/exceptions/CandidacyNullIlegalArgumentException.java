package com.plexus.profiler.models.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class CandidacyNullIlegalArgumentException extends Exception {

	private static final long serialVersionUID = 1L;

	public static final String DESCRIPTION = "Parametro invalido, tipo null";

	public static final int CODE = 007;

	public CandidacyNullIlegalArgumentException() {
		this("");
	}

	public CandidacyNullIlegalArgumentException(String detail) {
		super(DESCRIPTION + " " + detail + ". CODE: " + CODE);
	}

}
