package com.plexus.profiler.models.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class CandidacyParamArgumentTypeMismatchException extends Exception{

	private static final long serialVersionUID = 1L;

	private static final String DESCRIPTION = "Parametro invalido, formato no válido";
	
	private static final int CODE = 007;
	
	public CandidacyParamArgumentTypeMismatchException() {
		this("");
	}
	
	public CandidacyParamArgumentTypeMismatchException(String detail) {
		super(DESCRIPTION + " " + detail + ". CODE: " + CODE);
	}
}
