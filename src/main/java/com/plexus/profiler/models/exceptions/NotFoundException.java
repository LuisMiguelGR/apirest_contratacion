package com.plexus.profiler.models.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND) // Debería retornar 404
public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public NotFoundException(String msg) {
        super("Error: No existe el recurso con id " + msg + ".");
    }

    public NotFoundException(String msg, int aux){
        super(msg);
    }

}
