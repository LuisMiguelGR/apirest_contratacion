package com.plexus.profiler.models.repositories;

import org.springframework.data.repository.CrudRepository;

import com.plexus.profiler.models.entities.Candidacy;

public interface CandidacyRepository extends CrudRepository<Candidacy, Long>{

}
