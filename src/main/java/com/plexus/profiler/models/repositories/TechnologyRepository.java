package com.plexus.profiler.models.repositories;

import com.plexus.profiler.models.entities.Technology;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * TechnologyRepository
 */
@Repository
public interface TechnologyRepository extends CrudRepository<Technology, Long>{

    
}