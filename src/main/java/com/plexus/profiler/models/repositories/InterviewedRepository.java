package com.plexus.profiler.models.repositories;

import com.plexus.profiler.models.entities.Interviewed;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
/**
 * InterviewedRepository
 */
@Repository
public interface InterviewedRepository extends CrudRepository<Interviewed, Long> {

    
}
