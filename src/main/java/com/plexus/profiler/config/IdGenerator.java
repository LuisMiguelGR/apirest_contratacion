package com.plexus.profiler.config;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

/**
 * IdGenerator
 */
public class IdGenerator implements IdentifierGenerator {

	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		Connection connection = session.connection();

		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("Select count(ID) as Id from INTERVIEWEDS");
			if (rs.next()) {
				Long id = rs.getLong(1);
				Long generatedId = id + 1;
				return generatedId;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
	

}