package com.plexus.profiler.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


@EnableSwagger2
@Component
@Configuration
public class SwaggerConfig {
	@Bean
	public Docket postsApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.useDefaultResponseMessages(false)
				.select()
	            .apis(RequestHandlerSelectors
	                .basePackage("com.plexus.profiler.controller"))
	            .paths(PathSelectors.regex("/.*"))
	            .build().apiInfo(apiInfo());
	}


	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Aplicación Gestión Entrevistados API")
				.description("Aplicación Gestión Entrevistados API para desarrolladores")
				.termsOfServiceUrl("http://www.plexus.com")
				.contact(new Contact("Plexus Technologies", "www.plexus.com", "info@plexus.com")).build();
	}

}



