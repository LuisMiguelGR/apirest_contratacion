package com.plexus.profiler.profilers;


import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration.AccessLevel;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.transaction.annotation.EnableTransactionManagement;


@SpringBootApplication

@ComponentScan(basePackages= {"com.plexus.profiler.models.exceptions", "com.plexus.profiler.config","com.plexus.profiler.controllers", "com.plexus.profiler.services", "com.plexus.profiler.models.repositories", "com.plexus.profiler.models.daos"})


@EnableAutoConfiguration
@EntityScan(basePackages={"com.plexus.profiler.models.entities"})
//@EnableTransactionManagement


@EnableJpaRepositories({"com.plexus.profiler.models.repositories", "com.plexus.profiler.models.dtos"})

public class ProfilerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProfilerApplication.class, args);		
	}
	
	@Bean
	   public ModelMapper modelMapper() {
	      ModelMapper modelMapper = new ModelMapper();
//	      modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	      return modelMapper;
	   }

	
}
