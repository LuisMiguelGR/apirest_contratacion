package com.plexus.profiler.controllers;

import java.util.List;

import com.plexus.profiler.models.dtos.InterviewedDto;
import com.plexus.profiler.models.entities.Interviewed;
import com.plexus.profiler.models.exceptions.*;
import com.plexus.profiler.services.InterviewedService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ResponseHeader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

/**
 * InterviewedController
 */
@RestController
@RequestMapping("/entrevistados")
@Api(value = "Operaciones con entrevistados")
public class InterviewedController {

	@Autowired
	InterviewedService interService;
		

	// GET ENTREVISTADOS
	@GetMapping("")
	@Validated
	@ApiOperation(value = "Operacion getAllIntervieweds()", notes = "Devuelve una lista con todos los entrevistados que existen en la base de datos")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK! El resultado se ejecuto perfectamente.", response = Interviewed[].class, responseHeaders = {
					@ResponseHeader(name = "Entrevistadores totales", description = "Todos los entrevistados encontrados", response = Long.class) }),
			@ApiResponse(code = 404, message = "No encontrado. No se ha encontrado el recurso") })
	public ResponseEntity<List<InterviewedDto>> getAllIntervieweds() throws InterviewedIdNotFoundException {
		List<InterviewedDto> lista = this.interService.getAllEntrevieweds();
		return new ResponseEntity<List<InterviewedDto>>(lista, HttpStatus.OK);
		
	}

	// GET ENTREVISTADO POR ID
	@GetMapping("/{id}")
	@Validated
	@ApiOperation(value = "Operacion getInterviewed(id)", notes = "Devuelve el entrevistado con el id ingresado")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK! El resultado se ejecuto perfectamente.", response = Interviewed.class, responseHeaders = {
					@ResponseHeader(name = "Entrevistado encontrado", description = "Entrevistado encontrado con exito", response = Long.class) }),
			@ApiResponse(code = 400, message = "Parametro incorrecto, argumento ilegal"),
			@ApiResponse(code = 404, message = "No encontrado. No se ha encontrado el recurso"),
			@ApiResponse(code = 500, message = "El tipo de parametro no es el esperado") })
	public ResponseEntity<InterviewedDto> getInterviewed(
			@ApiParam(value = "Id del entrevistador a buscar", required = true, name = "ID") @PathVariable("id") Long id)
			throws InterviewedIdNotFoundException, MethodArgumentTypeMismatchException, IllegalArgumentException {
		InterviewedDto inter = this.interService.getInterviewedById(id);
		return new ResponseEntity<InterviewedDto>(inter, HttpStatus.OK);
	}

	// DELETE ENTREVISTADO CON ID
	@DeleteMapping("/{id}")
	@Validated
	@ApiOperation(value = "Operacion deleteInterviewed(id)", notes = "Borra el entrevistado con el id ingresado")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK! El resultado se ejecuto perfectamente.", response = Interviewed.class, responseHeaders = {
					@ResponseHeader(name = "Entrevistado borrado", description = "Entrevistado borrado con exito", response = Long.class) }), })
	public ResponseEntity<Interviewed> deleteInterviewed(
			@ApiParam(value = "Id del entrevistado a borrar", required = true, name = "ID") @PathVariable Long id) 
					throws InterviewedConstraintException, InterviewedIdNotFoundException, MethodArgumentTypeMismatchException, IllegalArgumentException {
		this.interService.deleteInterviewed(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	// ADD ENTREVISTADO 
	@PostMapping("")
	@Validated
	@ApiOperation(value = "Operacion newInterviewed(Interviewed)", notes = "Añade el entrevistado pasado por parametro")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK! El resultado se ejecuto perfectamente.", response = Interviewed.class, responseHeaders = {
					@ResponseHeader(name = "Entrevistado añadido", description = "Entrevistado añadido con exito", response = Long.class) }),
			@ApiResponse(code = 403, message = "El entrevistado ya se encuentra en la base de datos, se comprueba por DNI"),})
	public ResponseEntity<InterviewedDto> newInterviewed(@ApiParam(value="Objeto entrevistado a añadir", required=true, name="Interviewed") @RequestBody InterviewedDto inter)
			throws Exception {
		InterviewedDto newInter = this.interService.newInterviewed(inter);
		return new ResponseEntity<InterviewedDto>(newInter, HttpStatus.CREATED);
	}

	
	// UPDATE ENTREVISTADO CON ID
	@PutMapping("/{id}")
	@Validated
	@ApiOperation(value = "Operacion updateInterviewed(id)", notes = "Actualiza el entrevistado con el id ingresado")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK! El resultado se ejecuto perfectamente.", response = Interviewed.class, responseHeaders = {
					@ResponseHeader(name = "Entrevistado editado", description = "Entrevistado editado con exito", response = Long.class) }),
			@ApiResponse(code = 400, message = "Parametro incorrecto, argumento ilegal"),
			@ApiResponse(code = 404, message = "El entrevistado no se encontro en la base de datos"),
			@ApiResponse(code = 404, message = "El entrevistado ya se encuentra en la base de datos, se comprueba por DNI"),
			})
	public ResponseEntity<InterviewedDto> updateInterviewed(
			@ApiParam(value="Objeto entrevistado a actualizar", required=true, name="Interviewed") @RequestBody InterviewedDto interNew,
			@ApiParam(value="Id del entrevistado a actualizar", required=true, name="Id") @PathVariable("id") Long id
			) 
					throws Exception
			 {
		InterviewedDto updateInter = this.interService.updateInterviewed(interNew, id);
		return new ResponseEntity<InterviewedDto>(updateInter, HttpStatus.OK);
	}

	
	// DELETE TODOS LOS ENTREVISTADOS
	@DeleteMapping("/borrartodos")
	@Validated
	@ApiOperation(value = "Operacion deleteAllIntervieweds()", notes = "Elimina todos los entrevistados de la base de datos")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK! El resultado se ejecuto perfectamente.", response = Interviewed[].class, responseHeaders = {
					@ResponseHeader(name = "Entrevistados totales", description = "Todos los entrevistados se han eliminado", response = Long.class) }),
			@ApiResponse(code = 404, message = "No encontrado. No se ha encontrado el recurso") })
	public ResponseEntity<Interviewed> deleteAllIntervieweds() {
		this.interService.deleteAllIntervieweds();
		return new ResponseEntity<>(HttpStatus.OK);
	}

}