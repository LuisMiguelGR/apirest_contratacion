package com.plexus.profiler.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.plexus.profiler.models.dtos.CandidacyDto;
import com.plexus.profiler.models.entities.Candidacy;
import com.plexus.profiler.models.exceptions.CandidacyConstraintException;
import com.plexus.profiler.models.exceptions.CandidacyIdNotFoundException;
import com.plexus.profiler.services.CandidacyService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

/**
 * CandidacyController
 */
@RestController
@RequestMapping("/candidaturas")
@Api(value = "Operaciones con candidaturas")
public class CandidacyController {

	
	@Autowired
	CandidacyService candiService;

	// GET CANDIDATURAS
	@GetMapping("")
	@Validated
	@ApiOperation(value = "Operacion getAllCandidaciess()", notes = "Devuelve una lista con todos los candidaturas que existen en la base de datos")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK! El resultado se ejecuto perfectamente.", response = Candidacy[].class, responseHeaders = {
					@ResponseHeader(name = "Candidaturas totales", description = "Todos las candidaturas encontradas", response = Long.class) }),
			@ApiResponse(code = 404, message = "No encontrado. No se ha encontrado el recurso") })
	public ResponseEntity<List<CandidacyDto>> getAllCandidacies() throws CandidacyIdNotFoundException {
		List<CandidacyDto> lista = this.candiService.getAllCandidacies();
		return new ResponseEntity<List<CandidacyDto>>(lista, HttpStatus.OK);
	}

	// GET CANDIDATURA POR ID
	@GetMapping("/{id}")
	@Validated
	@ApiOperation(value = "Operacion getCandidacy(id)", notes = "Devuelve la candidatura con el id ingresado")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK! El resultado se ejecuto perfectamente.", response = Candidacy.class, responseHeaders = {
					@ResponseHeader(name = "Candidatura encontrado", description = "Candidatura encontrada con exito", response = Long.class) }),
			@ApiResponse(code = 400, message = "Parametro incorrecto, argumento ilegal"),
			@ApiResponse(code = 404, message = "No encontrado. No se ha encontrado el recurso"),
			@ApiResponse(code = 500, message = "El tipo de parametro no es el esperado") })
	public ResponseEntity<CandidacyDto> getCandidacy(
			@ApiParam(value = "Id de la candidatura a buscar", required = true, name = "ID") @PathVariable("id") Long id)
			throws CandidacyIdNotFoundException, MethodArgumentTypeMismatchException, IllegalArgumentException {
		CandidacyDto candi = this.candiService.getCandidacyById(id);
		return new ResponseEntity<CandidacyDto>(candi, HttpStatus.OK);
	}

	// DELETE CANDIDATURA CON ID
	@DeleteMapping("/{id}")
	@Validated
	@ApiOperation(value = "Operacion deleteCandidacy(id)", notes = "Borra la candidatura con el id ingresado")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK! El resultado se ejecuto perfectamente.", response = Candidacy.class, responseHeaders = {
					@ResponseHeader(name = "Candidatura borrado", description = "Candidatura borrada con exito", response = Long.class) }), })
	public ResponseEntity<Candidacy> deleteCandidacy(
			@ApiParam(value = "Id del candidatura a borrar", required = true, name = "ID") @PathVariable Long id) 
					throws CandidacyConstraintException, CandidacyIdNotFoundException, MethodArgumentTypeMismatchException, IllegalArgumentException {
		this.candiService.deleteCandidacy(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	// ADD CANDIDATURA 
	@PostMapping("")
	@Validated
	@ApiOperation(value = "Operacion newCandidacy(Candidacy)", notes = "Añade la candidatura pasada por parametro")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK! El resultado se ejecuto perfectamente.", response = Candidacy.class, responseHeaders = {
					@ResponseHeader(name = "Candidatura añadid", description = "Candidatura añadida con exito", response = Long.class) }),
			@ApiResponse(code = 403, message = "La candidatura ya se encuentra en la base de datos, se comprueba por ID"),})
	public ResponseEntity<CandidacyDto> newCandidacy(@ApiParam(value="Objeto candidatura a añadir", required=true, name="Candidacy") @RequestBody CandidacyDto candi)
			throws CandidacyConstraintException {
		CandidacyDto newCandi = this.candiService.newCandidacy(candi);
		return new ResponseEntity<CandidacyDto>(newCandi, HttpStatus.CREATED);
	}

	
	// UPDATE CANDIDATURA CON ID
	@PutMapping("/{id}")
	@Validated
	@ApiOperation(value = "Operacion updateCandidacy(id)", notes = "Actualiza la candidatura con el id ingresado")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK! El resultado se ejecuto perfectamente.", response = Candidacy.class, responseHeaders = {
					@ResponseHeader(name = "Candidatura editada", description = "Candidatura editada con exito", response = Long.class) }),
			@ApiResponse(code = 400, message = "Parametro incorrecto, argumento ilegal"),
			@ApiResponse(code = 404, message = "La candidatura no se encontro en la base de datos"),
			@ApiResponse(code = 404, message = "La candidatura ya se encuentra en la base de datos, se comprueba por ID"),
			})
	public ResponseEntity<CandidacyDto> updateCandidacy(
			@ApiParam(value="Objeto candidatura a actualizar", required=true, name="Candidacy") @RequestBody CandidacyDto candiNew,
			@ApiParam(value="Id de la candidatura a actualizar", required=true, name="Id") @PathVariable("id") Long id
			) 
					throws CandidacyConstraintException, CandidacyIdNotFoundException, MethodArgumentTypeMismatchException, IllegalArgumentException
			 {
		CandidacyDto updateCandi = this.candiService.updateCandidacy(candiNew, id);
		return new ResponseEntity<CandidacyDto>(updateCandi, HttpStatus.OK);
	}

	
	// DELETE TODAS LAS CANDIDATURAS
	@DeleteMapping("/borrartodas")
	@Validated
	@ApiOperation(value = "Operacion deleteAllCandidacies()", notes = "Elimina todas las candidaturas de la base de datos")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK! El resultado se ejecuto perfectamente.", response = Candidacy[].class, responseHeaders = {
					@ResponseHeader(name = "Candidaturas totales", description = "Todas las candidaturas se han eliminado", response = Long.class) }),
			@ApiResponse(code = 404, message = "No encontrado. No se ha encontrado el recurso") })
	public ResponseEntity<Candidacy> deleteAllCandidacies() {
		this.candiService.deleteAllCandidacies();
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
