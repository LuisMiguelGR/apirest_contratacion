package com.plexus.profiler.controllers;

import java.util.List;
// import java.util.Optional;

import javax.validation.Valid;

import com.plexus.profiler.models.dtos.TechnologyDto;
// import com.plexus.profiler.models.entities.Technology;
import com.plexus.profiler.models.exceptions.NotFoundException;
import com.plexus.profiler.models.exceptions.RecurrentException;
import com.plexus.profiler.services.TechnologyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

/**
 * TechnologyController
 */

@RestController
@RequestMapping("/technologies")
@Api(tags = "Technology Controller", description = "Muestra información sobre los métodos para obtener información o enviar información sobre las tecnologías.")
public class TechnologyController {

    @Autowired
    private TechnologyService technologyService;

    @ApiResponses(value = { @ApiResponse(code = 200, message = "200 OK al listar el contenido de la base de datos."),
            @ApiResponse(code = 204, message = "204 No content al no haber contenido en la base de datos.") })

    @ApiOperation(value = "Devuelve las tecnologías de desarrollo almacenadas en la base de datos.")
    @GetMapping("")
    public ResponseEntity<?> getAllTechnologies() {
        List<TechnologyDto> techs = this.technologyService.getAllTechnologies();
        if (techs.isEmpty()) {
            return new ResponseEntity<String>("No existen tecnologías actualmente.", HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<List<TechnologyDto>>(techs, HttpStatus.OK);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "200 OK al listar la tecnología del parámetro id de la base de datos."),
            @ApiResponse(code = 404, message = "404 Not Found al no haber tecnología de con ese id en la base de datos.") })
    @ApiOperation(value = "Devuelve la tecnologías de desarrollo según el id o lanza una excepción si no existe la tecnología con dicho id.")
    @GetMapping("/{id}")
    public ResponseEntity<TechnologyDto> getTechnologyById(
            @ApiParam(value = "Id necesario para obtener la tecnología deseada.") @RequestBody @Valid @PathVariable(name = "id") Long id) {
        TechnologyDto tech = technologyService.getTechnologyById(id);
        return new ResponseEntity<TechnologyDto>(tech, HttpStatus.OK);
    }

    @ApiResponses(value = { @ApiResponse(code = 201, message = "201 Created al crear con éxito una nueva tecnología."),
            @ApiResponse(code = 403, message = "403 Forbidden al intentar crear una tecnología de forma errónea.") })
    @ApiOperation(value = "Permite insertar una nueva tecnología de desarrollo.")
    @PostMapping("")
    public ResponseEntity<TechnologyDto> newTechnology(
            @ApiParam(value = "Tecnología necesaria para almacenarla en la base de datos.") @RequestBody TechnologyDto techP)
            throws RecurrentException {
        try {
            return new ResponseEntity<TechnologyDto>(this.technologyService.newTechnology(techP), HttpStatus.CREATED);
        } catch (DataIntegrityViolationException e) {
            throw new RecurrentException(techP.getName());
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "200 OK al editar con existo la tecnología con id pasando por parámetro."),
            @ApiResponse(code = 403, message = "403 Forbidden al intentar editar una tecnología de forma errónea.") })
    @ApiOperation(value = "Permite editar una nueva tecnología de desarrollo por id o lanza una excepción si no existe la tecnología con dicho id.")
    @PutMapping("/{id}")
    public ResponseEntity<TechnologyDto> editTechnology(
            @ApiParam(value = "Tecnología la cual va sobreescribir a la tecnología actual del id en cuestión.") @RequestBody TechnologyDto techP,
            @ApiParam(value = "Id necesario para editar la tecnología deseada.") @RequestBody @PathVariable(name = "id") long id)
            throws NotFoundException {
        TechnologyDto tech = technologyService.updateTechnology(techP, id);
        return new ResponseEntity<TechnologyDto>(tech, HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "200 OK al eliminar con existo la tecnología con id pasado por parámetro."),
            @ApiResponse(code = 404, message = "404 Not Found intentar borrar la tecnología pasada con id pasada por parámetro de forma errónea.") })
    @ApiOperation(value = "Permite elimiar una nueva tecnología de desarrollo por id o lanza una excepción si no existe la tecnología con dicho id.")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteTechnology(
            @ApiParam(value = "Id necesario para eliminar la tecnología deseada.") @RequestBody @PathVariable(name = "id") long id)
            throws NotFoundException {
        this.technologyService.deleteTechnologyById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiResponses(value = { @ApiResponse(code = 200, message = "200 OK al eliminar con existo todas las tecnologías."),
            @ApiResponse(code = 404, message = "404 Not Found intentar borrar todas las tecnologías las cuales no existen en la base de datos.") })
    @ApiOperation(value = "Permite elimiar todas las tecnologías almacenadas en la base de datos.")
    @DeleteMapping("/deleteall")
    public ResponseEntity<?> deleteAllTechnologies()
            throws NotFoundException {
        this.technologyService.deleteAllTechnologies();
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
